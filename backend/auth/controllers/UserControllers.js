
const { verify } = require('jsonwebtoken');
const {GetUser,AddUser, DeleteUser,UpdateUserInfo,PasswordChange} = require('../repository/userRepository');
const {GenerateToken,VarifyToken} = require('../userauth/userAuth')


//Get Profile of a particular user

function GetProfile(req,res){
    
    const data = VarifyToken(req.headers.authorization);
    GetUser(data.user).then(user=>{
        res.status(200).send(user);
    }).catch(data=>{
        res.status(404).send('User not found');
    })
}

//register a user

function RegisterUser(req,res){
   AddUser(req.body).then(data=>{
     res.status(200).send(data);
   }).catch(data=>{
    res.status(404).send(data)
   })

}

// login authenticate user
function LoginUser(req,res){
    const token =   GenerateToken(req.session.passport);
    const data=VarifyToken(req.headers.authorization);
    console.log(req.headers);
    console.log(data);
    id=data.user;
    console.log(id);
    res.cookie("jwt", token, { httpOnly: false ,maxAge: 600000 })
    res.status(200).send({
        userid:id,
        state:true,
        token:GenerateToken(req.session.passport),
    })
    

}





function ChangePassword(req,res){
   const data = VarifyToken(req.headers.authorization);
   PasswordChange(data.user,req.body).then(data=>{
    res.status(200).send(data);
   }).catch(data=>{
    res.status(404).send(data);
   })
  
  
    
 }

// to unregister a user from userbase
function UnregisterUser(req,res){
    DeleteUser(req.body).then( user=>{
        res.status(200).send(user);
    });
}

// to update User info

function UpdateProfile(req,res){
    const data = VarifyToken(req.headers.authorization);
    UpdateUserInfo(data.user,req.body).then(data=>{
        res.status(200).send(data);
    }).catch(data=>{
        res.status(404).send(data);
    })

}


  



module.exports = {GetProfile, RegisterUser,LoginUser , UnregisterUser,UpdateProfile,ChangePassword};