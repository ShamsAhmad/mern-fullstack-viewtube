import React from 'react'
import { Link, NavLink } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
 const SideBar=()=>{
    const navigate=useNavigate();
    return<>
    <div className="sidebar">
        
        <nav className="sidenav">
            <ul>
                <li>
                   {/* <button onClick={()=>{
                    navigate("/landing")
                   }}>Home</button> */}
                   <NavLink to="/landing">Home</NavLink>

                </li>
                <li>
                <NavLink to="/favourite">Favourite</NavLink>

                </li>
                <li>
                <NavLink to="/landing">Trending</NavLink>

                </li>
                <li>
                <NavLink to="/entertainment">Entertainment</NavLink>

                </li>
                <li>
                <NavLink to="/music">Music</NavLink>

                </li>
                <li>
                <NavLink to="/sports">Sports</NavLink>

                </li>
            </ul>
        </nav>
    </div>
    </>
 }
 export default SideBar;