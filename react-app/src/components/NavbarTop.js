import React from "react";
import styled from "styled-components";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import { Link } from "react-router-dom";
import { useState } from "react";
import axios from "axios";
import Dropdown from 'react-bootstrap/Dropdown';
//import "../components/navbar.css"
import logo from "../assets/logo.png"
import { Form } from "react-bootstrap"
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import viewtubeImg from "../assets/viewtube.png";

const NavbarTop = (props) => {
  const [search, setSearch] = useState("");
  function handleChange(e) {
    console.log(search);
    const { value, name } = e.target;
    setSearch(prev => {
      return { ...prev, [name]: value }
    })

  }
  async function handleSubmit(e) {
    e.preventDefault()
    console.log(search);
    // let data = await axios.get('http://localhost:5000/search',
    // { 
    //   params:{search_query:search} }
    // );
    props.onClick(search);
    // console.log(data);

  }
  return (
    
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
    <Container>
            <Navbar.Brand >View <img style={{ height: '35px', width: "35px" }} src={viewtubeImg} /> Tube</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    <Form className="d-flex"  onSubmit={handleSubmit}>
                    <Form.Control
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                        onChange={handleChange}
                       
                    />
<Button variant="outline-success" type="submit">Search</Button>
</Form>

                    </Nav>
                    <Nav>
                            {/* <Button variant="success" size="sm"  onClick={() => navigate('/Signin')}>Sign In</Button>  */}
                            {/* <Button variant="success" size="sm"  onClick={() => navigate('/Signup')}>Sign Up</Button>     */}
                            <Nav.Link href="/Signin">Sign In</Nav.Link>
                            <Nav.Link eventKey={2} href="/Signup">
                                    Sign Up
                            </Nav.Link>
                    </Nav>
            </Navbar.Collapse>
    </Container>
</Navbar>


  );
};

export default NavbarTop;
