import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import YouTube from 'react-youtube';
import axios from "axios";
const Container = styled.div`
  width: ${(props) => props.type !== "sm" && "360px"};
  margin-bottom: ${(props) => (props.type === "sm" ? "10px" : "45px")};
  cursor: pointer;
  display: ${(props) => props.type === "sm" && "flex"};
  gap: 10px;
`;

const Image = styled.img`
  width: 100%;
  height: ${(props) => (props.type === "sm" ? "120px" : "202px")};
  background-color: #999;
  flex: 1;
`;

const Details = styled.div`
  display: flex;
  margin-top: ${(props) => props.type !== "sm" && "16px"};
  gap: 12px;
  flex: 1;
`;

const ChannelImage = styled.img`
  width: 36px;
  height: 36px;
  border-radius: 50%;
  background-color: #999;
  display: ${(props) => props.type === "sm" && "none"};
`;

const Texts = styled.div``;

const Title = styled.h1`
  font-size: 16px;
  font-weight: 500;
  color: ${({ theme }) => theme.text};
`;

const ChannelName = styled.h2`
  font-size: 14px;
  color: ${({ theme }) => theme.textSoft};
  margin: 9px 0px;
`;

const Info = styled.div`
  font-size: 14px;
  color: ${({ theme }) => theme.textSoft};
`;

const Card = (props) => {
  console.log(props.videoId);

  async function addToFavourite(){
    
    let response = await axios.post('http://localhost:9000/api/favorite/addToFavorite',
    { 
      userid:localStorage.getItem("userid"),
      videoId:props.videoId,
      channelTitle:props.channelName,
      title:props.title,
    }
    );
  }
  const opts = {
    height: '250',
    width: '300',
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 0,
      fs:0,
      modestbranding:1,
     
      
      

    },
  };
  return ( 
    <>
    <Link to={`/video/${props.videoId}`} style={{ textDecoration: "none" }}>
      <Container type={props.type}>
      <YouTube videoId={props.videoId} opts={opts} showInfo="false" />;
        <Details type={props.type}>
          <Texts>
            <Title>{props.title}</Title>
            <ChannelName className="channel-title" style={{color:"black"}}>{props.channelName}</ChannelName>
          </Texts>
        </Details>
      </Container>
    </Link>
    <button onClick={addToFavourite}>Add</button>
    </>
  );
};

export default Card;
